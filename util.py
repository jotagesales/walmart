# -*- coding:UTF-8
__author__ = 'Jotage'

import json

from flask import Response

def response_json(data, message='', status_code=200):
    response_data = json.dumps(
        {
            'data':data,
            'message':message
        }
    )
    return Response(response_data, mimetype='application/json', status=status_code)