# -*- coding:UTF-8

__author__ = 'Jotage'

from flask import Flask
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.sqlite'

app.secret_key='secret'

db = SQLAlchemy(app)

#register blueprints
from blueprints_API.routes import bp_api

app.register_blueprint(bp_api)

if __name__ == '__main__':
    app.run(debug=True,use_reloader=True)