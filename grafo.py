# -*- coding:UTF-8
__author__ = 'Jotage'


class Grafo:
    def __init__(self):
        self.vizinhos = {}
        self.vertices = {}

    def add_vertice(self, vertice_name):
        """
        Add a new vertex in grafh
        :param vertice_name:
        """
        self.vertices[vertice_name] = True

    def add_aresta(self, source, destination, distance=None):
        """
        add arest in graph, is recomended set source id and destination id to ensure integrity
        :param source: Interger or String with vertice name or vertice ID
        :param destination: Integer or String with destination name or destination ID
        :param distance:
        """
        if not source in self.vizinhos:
            self.vizinhos[source] = []

        if not destination in self.vizinhos:
            self.vizinhos[destination] = []

        if distance:
            self.vizinhos[source].append({destination: distance})
            self.vizinhos[destination].append({destination: distance})

    def get_vertices_vizinhos(self, vertice):
        """
        returns a dictionary with all neighboring vertices
        exemple:

        {'A': 10, 'E': 50, 'D': 15}

        :param vertice: vertice name or vertice id
        :return: dict
        """
        dic_vizinhos = {}
        if vertice in self.vizinhos.keys():
            for vizinho in self.vizinhos.get(vertice):
                chave = vizinho.keys()[0]
                dic_vizinhos[chave] = vizinho.get(chave)

        return dic_vizinhos

    def __get_all_routes(self, source, destination, route=[]):
        """
        calculate all possibles routes
        :return : list of list
        """
        route = route + [source]

        if source == destination:
            return [route]

        possible_routes = []
        for vertice in self.vizinhos[source]:
            if vertice.keys()[0] not in route:
                new_routes = self.__get_all_routes(vertice.keys()[0], destination, route)

                for new_route in new_routes:
                    possible_routes.append(new_route)

        return possible_routes

    def get_best_route(self, source, destination):
        """
        Calculate the small route between possible routes
        :param source:origin
        :param destination: destination
        :return: :raise Exception:
        """
        vertices = self.vertices.keys()
        if source not in vertices or destination not in vertices:
            raise Exception(u'Não foi possível calcular a rota, pois um dos pontos não existe no mapa.')

        all_routes = self.__get_all_routes(source, destination)

        small_distance = 99999999
        best_route = []
        for route_list in all_routes:
            sum_distance = 0
            for index, vertice in enumerate(route_list):
                try:
                    next_vertice = route_list[index + 1]
                    distance = self.get_vertices_vizinhos(vertice)[next_vertice]

                    sum_distance += distance

                except IndexError:
                    if sum_distance < small_distance:
                        small_distance = sum_distance
                        best_route = route_list

        return best_route, small_distance
