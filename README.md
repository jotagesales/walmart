## Rodando a API localmente

Para rodar localmente é bem simples basta seguir os passos listados abaixo: 

1-Instalando virtualenv e o git:

Ubuntu:
```
sudo apt-get update
sudo apt-get install python-virtualenv git-core
```

CentOs:
```
yum update
yum install python-virtualenv git
```

2- rodando o projeto:
Acesse um diretório da sua preferência e aplique os seguintes comandos.

```
git clone https://gitlab.com/jotagesales/walmart.git
cd walmart/
virtualenv .venv
source .venv/bin/activate
pip install -r requirements.txt 
python app.py
```

Pronto, nesse momento a API já está respondendo no endereço http://127.0.0.1:5000.

## Consumindo a API:

Podemos testar o consumo de nossa API de várias formas, por exemplo:

- RestClient, PostMan, Curl

Nos nossos exemplos vamos usar o curl por estar presente na maioria das distribuições unix.

EndPoints:
Temos os seguintes endpoints:

### maps
exemplo de uso: http://127.0.0.1:5000/maps, é importante salientar que para enviar os dados a API é necessário alterar o Content-Type da requisição para application/json.

exemplo de json para criar um novo mapa:

```

curl -H "Content-Type: application/json" -X POST -d '{"name":"Mapa de São Paulo"}' http://127.0.0.1:5000/maps

```


método  | ação                             | path
------- | ---------------------------------|---------
GET     | lista todos os mapas cadastrados | http://127.0.0.1:5000/maps
GET     | lista o mapa com id passado      | http://127.0.0.1:5000/maps/id 
POST    | cria um novo mapa                | http://127.0.0.1:5000/maps
PUT     | atualiza o mapa com o id passado | http://127.0.0.1:5000/maps/id
DELETE  | apagando um mapa                 | http://127.0.0.1:5000/maps/id


### Places

Esse endpoint é destinado a provê serviço para os pontos.

método  | ação                              | path
------- | ----------------------------------|---------
GET     | lista todos os locais cadastrados | http://127.0.0.1:5000/places
GET     | lista o local com id passado      | http://127.0.0.1:5000/places/id 
POST    | cria um novo local                | http://127.0.0.1:5000/places
PUT     | atualiza o local com o id passado | http://127.0.0.1:5000/places/id
DELETE  | apagando um local                 | http://127.0.0.1:5000/places/id

exemplo de json para criar um novo local:
```
curl -H "Content-Type: application/json" -X POST -d '{"name":"Ponto A","map_id":1}' http://127.0.0.1:5000/places
```

### Distances

Esse endpoint é destinado a provê serviço para cadastrar as distancias entre cada ponto.

método  | ação                                  | path
------- | --------------------------------------|---------
GET     | lista todas as distâncias cadastradas | http://127.0.0.1:5000/distances
GET     | lista a distância com id passado      | http://127.0.0.:5000/distances/id 
POST    | cria uma nova distancia               | http://127.0.0.1:5000/distances
PUT     | atualiza a distancia com o id passado | http://127.0.0.1:5000/distances/id
DELETE  | apagando uma distância                | http://127.0.0.1:5000/distances/id

exemplo de json para criar uma nova distância:
```

curl -H "Content-Type: application/json" -X POST -d '{"source_id":1,"destination_id":2,"distance":2}' http://127.0.0.1:5000/distances
```

### Routes

Esse endpoint é destinado a provê serviço para calcular a melhor rota, entre a origem e o destino.

método  | ação                                  | path
------- | --------------------------------------|---------
POST    | calcula melhor rota                   | http://127.0.0.1:5000/routes

exemplo de json para retornar o calculo da melhor rota.
```
curl -H "Content-Type: application/json" -X POST -d '{"source_id":1,"destination_id":5,"autonomy":10,"value":3.75}' http://127.0.0.1:5000/routes
```