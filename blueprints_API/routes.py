# -*- coding:UTF-8
__author__ = 'Jotage'

from flask import Blueprint

bp_api = Blueprint('api', __name__)

from views import MapAPI, PlaceAPI, DistanceAPI, RouteAPI

#register urls
map_view = MapAPI.as_view('map_api')
bp_api.add_url_rule('/maps', defaults={'map_id': None},view_func=map_view, methods=['GET',])
bp_api.add_url_rule('/maps', view_func=map_view, methods=['POST',])
bp_api.add_url_rule('/maps/<int:map_id>', view_func=map_view,methods=['GET', 'PUT', 'DELETE'])

place_view = PlaceAPI.as_view('place_api')
bp_api.add_url_rule('/places', defaults={'place_id': None},view_func=place_view, methods=['GET',])
bp_api.add_url_rule('/places', view_func=place_view, methods=['POST',])
bp_api.add_url_rule('/places/<int:place_id>', view_func=place_view,methods=['GET', 'PUT', 'DELETE'])

distance_view = DistanceAPI.as_view('distance_api')
bp_api.add_url_rule('/distances', defaults={'distance_id': None},view_func=distance_view, methods=['GET',])
bp_api.add_url_rule('/distances', view_func=distance_view, methods=['POST',])
bp_api.add_url_rule('/distances/<int:distance_id>', view_func=distance_view,methods=['GET', 'PUT', 'DELETE'])

route_view = RouteAPI.as_view('route_api')
bp_api.add_url_rule('/routes', view_func=route_view, methods=['POST',])