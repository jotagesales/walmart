# -*- coding:UTF-8
__author__ = 'Jotage'

from app import db, app

logger = app.logger

class BaseModelMixin:

    def save(self):
        try:

            db.session.add(self)
            db.session.commit()
            logger.info('salvo com sucesso')
        except Exception as e:
            logger.error(e)

        return self.to_json()

    def delete(self):
        try:

            db.session.delete(self)
            db.session.commit()

        except Exception as e:
            print e

    def to_json(self):
        raise NotImplemented

class Map(db.Model, BaseModelMixin):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150), nullable=False)

    places = db.relationship('Place',backref=db.backref('map'))

    def add_places(self, place_list=[]):
        try:

            if place_list:
                for place_name in place_list:
                    place = Place(name=place_name)

                    self.places.append(place)

            db.session.commit()

        except Exception as e:
            print e

        return self.to_json()


    def to_json(self):
        json = {
            'id':self.id,
            'name':self.name
        }
        return json


class Place(db.Model, BaseModelMixin):

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150), nullable=False)

    map_id = db.Column(db.Integer, db.ForeignKey('map.id'))

    def to_json(self):
        json = {
            'id':self.id,
            'name':self.name,
            'map_id':self.map_id
        }
        return json


class Distance(db.Model, BaseModelMixin):

    id = db.Column(db.Integer, primary_key=True)

    source_id = db.Column(db.Integer, db.ForeignKey('place.id'))

    destination_id = db.Column(db.Integer, db.ForeignKey('place.id'))

    distance = db.Column(db.Float, nullable=False)

    def add_distance(self, source_id, destination_id, distance):
        try:
            if source_id and destination_id and distance:
                source = Place.query.filter_by(id=source_id).first()
                destination = Place.query.filter_by(id=destination_id).first()

                if source and destination:
                    self.source_id = source.id
                    self.destination_id = destination.id
                    self.distance = distance

                    distance = self.save()

        except Exception as e:
            print e

        return distance

    def to_json(self):
        json = {
            'id':self.id,
            'source_id':self.source_id,
            'destination_id':self.destination_id,
            'distance':self.distance
        }
        return json

#create tables
db.create_all()