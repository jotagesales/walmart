# -*- coding:UTF-8

__author__ = 'Jotage'

from flask import request, jsonify
from flask.views import MethodView

from models import Map, Place, Distance
from util import response_json

from grafo import Grafo


class BaseView(MethodView):

    def __init__(self, *args, **kwargs):

        self.message = ''
        self.status_code = 200

        super(BaseView, self).__init__(*args, **kwargs)


class MapAPI(BaseView):

    def get(self, map_id):
        if map_id:
            maps = Map.query.filter_by(id=map_id).first()
            if not maps:
                self.message = u'Nao foi possivel encontrar o mapa com o id {}'.format(map_id)
                return response_json(maps,message=self.message)

            maps = maps.to_json()
        else:
            maps = [map.to_json() for map in Map.query.all()]
        return response_json(maps,message=self.message)

    def post(self):
        data = request.get_json(silent=True)
        if data:
            name = data.get('name','').strip()
            map = Map(name= name)
            map = map.save()
            self.message= 'Mapa salvo com sucesso!'
        else:
            self.message = u'Dados inválidos'
            self.status_code = 400

        return response_json(map, message=self.message, status_code=self.status_code)


    def put(self, map_id):
        data = request.get_json(silent=True)
        if data:
            name = data.get('name','').strip()

            if name and map_id:
                map = Map.query.filter_by(id=map_id).first()

                if map:
                    map.name = name
                    map = map.save()

                    self.message= 'Mapa salvo com sucesso!'
        else:
            self.message = u'Dados inválidos'
            self.status_code = 400

        return response_json(map, message=self.message, status_code=self.status_code)


    def delete(self, map_id):
        if map_id:
            map = Map.query.filter_by(id=map_id).first()

            if map:
                map = map.delete()
                self.message= 'Mapa deletado com sucesso!'
            else:
                self.message= u'Nao foi possivel encontrar o mapa com o id {}'.format(map_id)
                self.status_code = 400
        else:
            self.message = u'É necessário informar o id do mapa'
            self.status_code = 400

        return response_json(map, message=self.message, status_code=self.status_code)


class PlaceAPI(BaseView):

    def get(self, place_id):
        if place_id:
            place = Place.query.filter_by(id=place_id).first()

            if not place:
                self.message = u'Nao foi possivel encontrar o local com o id {}'.format(place_id)
                return response_json(place,message=self.message)

            place = place.to_json()
        else:
            place = [place.to_json() for place in Place.query.all()]
        return response_json(place)

    def post(self):
        data = request.get_json(silent=True)
        if data:
            name=[data.get('name','')]
            map_id = data.get('map_id')

            map = Map.query.filter_by(id=map_id).first()

            if map:
                map.add_places(name)
                self.message = 'Local criado com sucesso!'

            else:
                self.message =u'Nao foi possivel encontrar o local com o id {}'.format(map_id)
                return response_json(None, message=self.message, status_code=400)

        return response_json(None, message=self.message,status_code=201)

    def put(self, place_id):
        data = request.get_json(silent=True)
        if data:
            name = data.get('name','').strip()

            if name and place_id:
                place = Place.query.filter_by(id=place_id).first()

                if place:
                    place.name = name
                    place = place.save()

                    self.message= 'Mapa salvo com sucesso!'
        else:
            self.message = u'Dados inválidos'
            self.status_code = 400

        return response_json(place, message=self.message, status_code=self.status_code)

    def delete(self, place_id):
        if place_id:
            place = Place.query.filter_by(id=place_id).first()

            if place:
                place = place.delete()
                self.message= 'Local deletado com sucesso!'
            else:
                self.message= u'Nao foi possivel encontrar o local com o id {}'.format(place_id)
                self.status_code = 400
        else:
            self.message = u'É necessário informar o id do local'
            self.status_code = 400

        return response_json(place, message=self.message, status_code=self.status_code)


class DistanceAPI(BaseView):

    def get(self, distance_id):
        if distance_id:
            distance = Distance.query.filter_by(id=distance_id).first().to_json()
        else:
            distance = [distance.to_json() for distance in Distance.query.all()]
        return response_json(distance)

    def post(self):
        data = request.get_json(silent=True)
        if data:
            source_id = int(data.get('source_id'))
            destination_id = int(data.get('destination_id'))
            distance_km = float(data.get('distance'))

            try:
                distance = Distance()
                distance_json = distance.add_distance(source_id, destination_id, distance_km)
            except Exception as e:
                print e

        return response_json(distance_json, status_code=201)

    def put(self, distance_id):
        data = request.get_json(silent=True)
        if data:
            source_id = int(data.get('source_id','').strip())
            destination_id = int(data.get('destination_id','').strip())
            distance_km = float(data.get('distance','').strip())

            if source_id and destination_id and distance_km:
                distance = Distance.query.filter_by(id=distance_id).first()

                source = Place.query.filter_by(id=source_id).first()
                destination = Place.query.filter_by(id=destination_id).first()

                if distance and source and destination:
                    distance.source_id = source.id
                    distance.destination_id = destination.id
                    distance.distance = distance_km

                    distance = distance.save()

                    self.message= u'Distancia salva com sucesso!'
        else:
            self.message = u'Dados inválidos'
            self.status_code = 400

        return response_json(distance, message=self.message, status_code=self.status_code)

    def delete(self, distance_id):
        if distance_id:
            distance = Distance.query.filter_by(id=distance_id).first()

            if distance:
                distance = distance.delete()
                self.message= 'Distancia deletada com sucesso!'
            else:
                self.message= u'Nao foi possivel encontrar a distancia com o id {}'.format(distance_id)
                self.status_code = 400
        else:
            self.message = u'É necessário informar o id da distancia'
            self.status_code = 400

        return response_json(distance, message=self.message, status_code=self.status_code)


class RouteAPI(BaseView):

    def post(self):
        data = request.get_json(silent=True)
        if data:
            source_id = data.get('source_id','')
            destination_id = data.get('destination_id','')
            autonomy = float(data.get('autonomy',0))
            value = float(data.get('value',0))

            source = Place.query.filter_by(id=source_id).first()
            destination = Place.query.filter_by(id=destination_id).first()

            if source and destination:
                if source.map.id == destination.map.id:
                    vertices = source.map.places

                    grafo = Grafo()
                    for vertice in vertices:
                        #using id for vertice for integrit
                        grafo.add_vertice(vertice.id)

                        distances = Distance.query.filter_by(source_id=vertice.id).all()
                        for distance in distances:
                            grafo.add_aresta(distance.source_id, distance.destination_id, distance.distance)

                route_list, distance = grafo.get_best_route(source.id, destination.id)

                if route_list:
                    route = []
                    for place_id in route_list:
                        place = Place.query.get(place_id)
                        route.append(place.name)

            else:
                 self.message = u'A rota não pode ser calculada, pois a origem e o destino fazem parte de mapas ' \
                                   u'diferentes'

                 return response_json(None, self.message, 400)

        if autonomy > 0 and value > 0:
            cost = round(distance /(autonomy / value), 2)
            data = {'route':route, 'distance':distance,'cost':cost}
            self.message = u'Rota calculada com sucesso!'
        else:
            self.message = 'Os campos autonomy e value devem ser maiores que zero.'
            return response_json(None, self.message, status_code=400)

        return response_json(data, self.message)
